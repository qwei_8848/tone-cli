#!/usr/bin/env python
# -*- coding: utf-8 -*-


from utils.log import logger
from module_interfaces import JOBPRE


class Pre(JOBPRE):
    priority = 0

    def pre(self, job_instance):
        logger.debug("Pre job")
