<div align="center">
<h1>tone command line</h1>
</div>

简体中文 | [English](README.md)

## 安装

```bash
make install
```

## 使用

### 查看使用帮助

    tone --help/-h

### 命令说明

    tone [-h] [-v] {list,run,fetch,install,uninstall,archive}{testsuite_name}

- tone list：展示支持的testsuite列表
- tone list testsuite_name：展示指定testsuite下的全部testconf
- tone run testsuite_name：运行指定testsuite下的全部testconf
- tone run testconf_name：运行指定testconf
- tone fetch testsuite_name：拉取指定testsuite，会根据tests/testsuite/install.sh中的GIT_URL/WEB_URL获取远程文件
- tone install testsuite_name：安装指定testsuite
- tone uninstall testsuite_name：卸载指定testsuite
- tone archive：离线测试，在特定网络环境中，可以手动运行测试后，通过此命令将测试结果打为tar包，上传至T-One平台后平台上进行分析



## 更多文档

[运行测试工具的完整过程](docs/manual-zh_CN.md)

[如何为tone-cli集成新的测试工具](docs/add-testsuite-zh_CN.md)