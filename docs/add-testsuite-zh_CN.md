# 如何增加测试套件

## 创建文件（目录）

```bash
mkdir /tests/$benchmark_name/
touch /tests/$benchmark_name/install.sh # 用于安装
touch /tests/$benchmark_name/run.sh # 用于运行
touch /tests/$benchmark_name/README.md # 测试套说明
```

## 文件内容填充

### install.sh

- $GIT_URL/$WEB_URL：git clone或wget方式可用的包安装路径
- BRANCH：分支名
- DEP_PKG_LIST：依赖包列表
- build()：设置编译环境，编译测试套件
- install()：安装测试套件

### run.sh

- setup()：运行前要执行的设置任务
- run()：运行步骤
- teardown()：运行后的清理工作
- parse()：测试结果解析，以键值对形式输出

### README.md

- 测试套件描述
- 测试套件使用说明
- 版本号
- 运行参数说明
- 结果说明
- 手动运行方法
