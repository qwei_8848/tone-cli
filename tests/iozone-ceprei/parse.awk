#!/usr/bin/awk -f

/kB  reclen    write  rewrite    read    reread    read     write     read   rewrite      read   fwrite frewrite    fread  freread/{
getline
printf("kB: %d\n",$1)
printf("reclen: %d\n",$2)
printf("write: %d\n",$3)
printf("rewrite: %d\n",$4)
printf("read: %d\n",$5)
printf("reread: %d\n\n",$6)
}
