
run()
{
    export PATH="$PATH:$TONE_BM_RUN_DIR/bin"
    taskset -c $task_set_cpu_list ./cyclictest -l $loop_num -p $priority -t $thread_num -h $histogram -m -s -a $affinity
}

parse()
{
    python $TONE_BM_SUITE_DIR/parse.py
}