#!/bin/bash

GIT_URL="git://git.kernel.org/pub/scm/utils/rt-tests/rt-tests.git"
BRANCH="main"

if [[ "ubuntu debian uos kylin" =~  $TONE_OS_DISTRO ]];then
    DEP_PKG_LIST="libnuma-dev"
else
    DEP_PKG_LIST="numactl-devel"
fi

fetch()
{
    cd $TONE_BM_CACHE_DIR
	rm -rf rt-tests
}

build()
{
	cd $TONE_BM_BUILD_DIR/rt-tests
    make
}

install()
{
    rm -f $TONE_BM_RUN_DIR/bin/cyclictest
	cp $TONE_BM_BUILD_DIR/rt-tests/cyclictest $TONE_BM_RUN_DIR/bin
}