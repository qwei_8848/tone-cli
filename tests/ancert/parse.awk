#!/usr/bin/awk -f

BEGIN {
    nr_test = 0
}

/TEST PASS!/ {
    if ($1 != "+") {
        printf("%s: Pass\n", $1)
        nr_test += 1
    }
}

/TEST FAIL!/ {
    printf("%s: Fail\n", $1)
    nr_test += 1
}

/TEST SKIP!/ {
    printf("%s: Skip\n", $1)
    nr_test += 1
}
