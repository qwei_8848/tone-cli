#!/bin/env python3
import sys
import re

def futex_parse(futexlog):
    for line_futex in futexlog:
        if re.match(r'(.*# futex_|.*# futex.)', line_futex):
            sub_futex=line_futex.split(':')[0].split()[-1]
        if re.match(r'(.*Arguments:)', line_futex):
            sub_futex = sub_futex + '.' + line_futex.split(": ")[1].replace(" ", ".").strip()
        if re.match(r'(.*ok )', line_futex):
            print('%s.%s: %s' % ('futex', sub_futex, 'Pass'))
        if re.match(r'(.*not ok )', line_futex):
            print('%s.%s: %s' % ('futex', sub_futex, 'Fail'))
        if re.match(r'(TAP version 13)', line_futex):
            return

def vm_parse(vmlog):
    for line_vm in vmlog:
        if re.match(r'(.*run_vmtests)', line_vm):
            continue
        if re.match(r'(.*running*)', line_vm):
            sub_vm=line_vm.split('running')[-1].strip().replace(' ','_')
        if re.match(r'(.*PASS])', line_vm):
            print('%s.%s: %s' % ('vm', sub_vm, 'Pass'))
        if re.match(r'(.*FAIL])', line_vm):
            print('%s.%s: %s' % ('vm', sub_vm, 'Fail'))
        if re.match(r'(.*SKIP])', line_vm):
            print('%s.%s: %s' % ('vm', sub_vm, 'Skip'))
        if re.match(r'(TAP version 13)', line_vm):
            return

with sys.stdin as stdin:
    for line in stdin:
        if re.match(r'(.*selftests: futex: run.sh)', line):
            futex_parse(stdin)
        if re.match(r'(.*selftests: vm: run_vmtests)', line):
            vm_parse(stdin)
        if re.match(r'(ok.*selftests:)', line):
            testname = line.split(': ')[-2]
            subtest = line.split(': ')[-1].split()[0]
            tail_str = line.split()[-1].strip()
            if tail_str in '[SKIP]':
                status = 'Skip'
            else:
                status = 'Pass'
            print('%s.%s: %s' % (testname, subtest, status))
        if re.match(r'(not ok.*selftests:)', line):
            testname = line.split(': ')[-2]
            subtest = line.split(': ')[-1].split()[0]
            tail_str = line.split()[-1].strip()
            if tail_str in '[SKIP]':
                status = 'Skip'
            else:
                status = 'Fail'
            print('%s.%s: %s' % (testname, subtest, status))
        if re.match(r'(ignored_by_tone )', line):
            casename = line.split()[1].split(':')[0]
            status = line.split()[-1].strip()
            print('%s: %s' % (casename, status))
