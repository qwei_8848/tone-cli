#!/bin/bash
export PATH=$TONE_BM_RUN_DIR/bin:$PATH
server_cmd="$NUMACTL sockperf server"

pkill -f "sockperf server" &>/dev/null
echo "$(date "+%F %T") $server_cmd &"
$server_cmd &
echo "$(date "+%F %T") $server_cmd --tcp &"
$server_cmd --tcp &
