#!/bin/bash

run()
{
	cd "$TONE_BM_RUN_DIR" ||exit

	numa_node_total_num=$(lscpu | awk '/^NUMA node\(s\):/{print $3}')
	socket_num=$(lscpu | awk '/^Socket\(s\):/{print $2}')
	l3_cache_kb_percpu=$(lscpu | awk '/^L3 cache:/{print $3}' | sed 's/K//')
	l3_cache_bytes_all=$(($l3_cache_kb_percpu * $socket_num * 1024))


	STREAM_ARRAY_SIZE=$(echo "$l3_cache_bytes_all * 4.1 /8" | bc)
	echo "STREAM_ARRAY_SIZE: $STREAM_ARRAY_SIZE"
	
	if [ $test == 'full_thread' ];then
		gcc -O3 stream.c -o stream -D"STREAM_ARRAY_SIZE=$STREAM_ARRAY_SIZE" -fopenmp
	else
		gcc -O3 stream.c -o stream -D"STREAM_ARRAY_SIZE=$STREAM_ARRAY_SIZE"
	fi
	./stream
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.awk
}
