# image-ci-test

## Description

Smoke test suite for container image.

## Homepage

[https://gitee.com/anolis/anolis-ci-test.git]

## Version

v0.1

## Category

Functional

## Results

```
container_startup_test: Pass
application_container_test: Pass

```
