#!/bin/bash

TONE_CAN_SKIP_PKG="yes"
DEP_PKG_LIST="python36 python36-devel rust nginx openssl-devel git kernel-headers"

nginx_conf=/etc/nginx/nginx.conf


check_kernel_headers()
{
    rpm -qa|grep kernel-headers-$(uname -r) && return 0
    local version=$(yum list --showduplicates  kernel-headers-`uname -r` |tail -1|awk '{print $2}')
    [ -n "$version" ] && yum install -y kernel-headers-$version && return 0 || return 1
}

set_python_env()
{
    pip3 install --upgrade pip
    pip3 install pynginxconfig
    pip3 install tornado==6.1
}

check_rust()
{
    rpm -q rust && return 0
    rpm -q epel-release || yum install -y -b test epel-release
    yum install rust -y && return 0 || return 1
}

set_nginx_env()
{
    [ -s ${nginx_conf}_bak ] || cp $nginx_conf ${nginx_conf}_bak
    # listen       [::]:80;
    sed -i "/^\s\+listen\s\+\[::\]/d" $nginx_conf
    # access_log  /var/log/nginx/access.log  main;
    sed -i "s/access_log.*/access_log off;/g" $nginx_conf
    systemctl restart nginx
    systemctl status nginx || return 1
}

set_env()
{
    check_kernel_headers
    check_rust
    set_python_env
}

build()
{
    set_env
    set_nginx_env
}

install()
{
    echo "install server finshed......."
}
