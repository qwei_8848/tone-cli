#!/usr/bin/awk -f

/^\[TestResult\].* PASSED/ {
	tc=$(NF-1)
	printf("%s: pass\n", tc)
}

/^\[TestResult\].* FAILED/ {
	tc=$(NF-1)
	printf("%s: fail\n", tc)
}

/^\[TestResult\].* SKIPPED/ {
	tc=$(NF-1)
	printf("%s: skip\n", tc)
}
