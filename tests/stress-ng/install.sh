WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/stress-ng-0.15.00.tar"

build()
{
    cd $TONE_BM_BUILD_DIR/stress-ng-0.15.00
    make || return
}

install()
{
    make install
}
